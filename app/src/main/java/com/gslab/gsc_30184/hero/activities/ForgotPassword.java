package com.gslab.gsc_30184.hero.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gslab.gsc_30184.hero.R;

public class ForgotPassword extends AppCompatActivity {

    private Button sendPassword;
    private EditText username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        sendPassword = findViewById(R.id.sendpassword);
        username = findViewById(R.id.username);
        sendPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (username.length() < 10)
                    Toast.makeText(getApplicationContext(), "incorrect email ", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(getApplicationContext(), "password sent", Toast.LENGTH_LONG).show();
            }
        });
    }
}
