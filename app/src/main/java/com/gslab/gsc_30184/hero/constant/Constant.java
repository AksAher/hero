package com.gslab.gsc_30184.hero.constant;

/**
 * Created by GSC-30184 on 20-02-2018.
 */

public class Constant {
    public static final int REQUEST_SIGN_UP = 0;
    public static final int REQUEST_ADD_PRODUCT = 1000;
    public static final int RESULT_LOAD_IMAGE = 2000;
    public static final int RESULT_ADD_PRODUCT = 5000;
    public static final int REQUEST_EDIT_PRODUCT = 1111;
    public static final int RESULT_EDIT_PRODUCT = 5555;
    public static final int REQUEST_SIGN_IN = 500;
    public static final String INTENT_EDIT_FLAG = "EditFlag";
    public static int SPLASH_TIME_OUT = 3000;
}
