package com.gslab.gsc_30184.hero.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gslab.gsc_30184.hero.R;


public class SignUpActivity extends AppCompatActivity {
    private static final String TAG = "SignUpActivity";
    private EditText firstNameText, emailText, mobText, confirmPasswordText, passwordText;
    private TextView loginLink;
    private Button signUpButton, signInButton;
    private String firstName, lastName, eMail, mobileNo, password, confirmPassword;
    private Spinner bloodGroup, gender;
    private String[] bloodGroupArray;
    private String[] GenderArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        firstNameText = findViewById(R.id.input_first_name);
        emailText = findViewById(R.id.input_email);
        confirmPasswordText = findViewById(R.id.input_confirm_password);
        passwordText = findViewById(R.id.input_password);
        mobText = findViewById(R.id.input_mobile_no);
        signUpButton = findViewById(R.id.btn_signUp);
        signInButton = findViewById(R.id.btn_signIn);
        bloodGroup = findViewById(R.id.blood_group_spinner);
        gender = findViewById(R.id.gender_spinner);


        firstNameText.setText("Ashish");
        emailText.setText("ash@gmail.com");
        mobText.setText("9545979692");


        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/MuseoSans.otf");
        firstNameText.setTypeface(custom_font);
        emailText.setTypeface(custom_font);
        mobText.setTypeface(custom_font);
        passwordText.setTypeface(custom_font);
        confirmPasswordText.setTypeface(custom_font);
        signInButton.setTypeface(custom_font);
        signUpButton.setTypeface(custom_font);


        this.bloodGroupArray = new String[]{
                "Blood Group--", "A+", "B+", "A-", "B-", "O", "AB"
        };
        ArrayAdapter<String> BGadapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, bloodGroupArray);
        bloodGroup.setAdapter(BGadapter);


        this.GenderArray = new String[]{
                "Gender--", "Male", "Female"
        };
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, GenderArray);
        gender.setAdapter(genderAdapter);


        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }


    public void signUp() {
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        signUpButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Verifying Account... OTP will be sent to your registered mobile number");
        progressDialog.show();


        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onSignupSuccess or onSignupFailed
                        // depending on success
                        onSignupSuccess();
                        // onSignupFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }


    public void onSignupSuccess() {
        signUpButton.setEnabled(true);
        Intent intent = new Intent(SignUpActivity.this, OTPActivity.class);
        startActivity(intent);
        finish();


    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Sign up failed", Toast.LENGTH_LONG).show();
        signUpButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;


        firstName = firstNameText.getText().toString();
        mobileNo = mobText.getText().toString();
        eMail = emailText.getText().toString();
        confirmPassword = confirmPasswordText.getText().toString();
        password = passwordText.getText().toString();

        if (firstName.isEmpty() || firstName.length() < 3) {
            firstNameText.setError("at least 3 characters");
            valid = false;
        } else {
            firstNameText.setError(null);
        }

        if (eMail.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(eMail).matches()) {
            emailText.setError("enter a valid email address");
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (mobileNo.isEmpty() || !Patterns.PHONE.matcher(mobileNo).matches()) {
            mobText.setError("Enter  mobile number");
            valid = false;
        } else {
            mobText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        if (confirmPassword.isEmpty() || !confirmPassword.matches(password)) {
            confirmPasswordText.setError("password mismatching");
            valid = false;
        } else {
            confirmPasswordText.setError(null);
        }

        return valid;
    }
}