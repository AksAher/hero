package com.gslab.gsc_30184.hero.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gslab.gsc_30184.hero.R;
import com.gslab.gsc_30184.hero.constant.Constant;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {
    Constant constant;
    private Button fbLoginButton, gmailLoginButton, loginButton, signupButton;
    private Pattern regexPattern;
    private Matcher regMatcher;
    private EditText editName, editPassword;
    private String stringUserName, stringPass;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.setTitle("Login");

        editName = findViewById(R.id.username);
        editPassword = findViewById(R.id.password);
        loginButton = findViewById(R.id.signin);
        signupButton = findViewById(R.id.signup);
        fbLoginButton = findViewById(R.id.fblogin);
        gmailLoginButton = findViewById(R.id.gmaillogin);
        TextView textViewForgotPassword = findViewById(R.id.forgotpassword);

        CheckBox mCbShowPwd;
        mCbShowPwd =  findViewById(R.id.cbShowPwd);
        mCbShowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (!isChecked) {
                    editPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {

                    editPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });
        textViewForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(getApplicationContext(), ForgotPassword.class);
                startActivity(newIntent);
            }
        });
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stringUserName = editName.getText().toString();
                if (stringUserName.length() < 10)
                    Toast.makeText(getApplicationContext(), "length at least 10", Toast.LENGTH_LONG).show();
                else {
                    stringPass = editPassword.getText().toString();
                    if (stringUserName.equals(stringPass)) {
                        Toast.makeText(getApplicationContext(), "Login successful", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                        startActivity(intent);
                    } else
                        Toast.makeText(getApplicationContext(), "incorrect password", Toast.LENGTH_LONG).show();
                }
            }
        });
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "signup pressed", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        gmailLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "pressed", Toast.LENGTH_LONG).show();

                Intent newIntent = new Intent(getApplicationContext(),GmailLoginActivity.class);
                startActivity(newIntent);
            }
        });
        fbLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(getApplicationContext(),FacebookLogin.class);
                startActivity(newIntent);
            }
        });
    }
}
