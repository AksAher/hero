package com.gslab.gsc_30184.hero.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gslab.gsc_30184.hero.R;


public class OTPActivity extends AppCompatActivity implements TextWatcher {

    StringBuilder sb = new StringBuilder();
    private EditText ed1, ed2, ed3, ed4;
    private Button btn_verify;
    private TextView otptxt;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        ed1 = findViewById(R.id.ed1);
        ed2 = findViewById(R.id.ed2);
        ed3 = findViewById(R.id.ed3);
        ed4 = findViewById(R.id.ed4);
        otptxt = findViewById(R.id.OTPtxt);

        btn_verify = findViewById(R.id.verifyOTP);


        ed1.setText("1");
        ed2.setText("2");
        ed3.setText("3");
        ed4.setText("4");
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/MuseoSans.otf");

        ed1.setTypeface(custom_font);
        ed2.setTypeface(custom_font);
        ed3.setTypeface(custom_font);
        ed4.setTypeface(custom_font);
        otptxt.setTypeface(custom_font);
        btn_verify.setTypeface(custom_font);


        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OTPActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        if (sb.length() == 1) {

            sb.deleteCharAt(0);

        }
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        switch (view.getId()) {
            case R.id.ed1:
                if (ed1.getText().toString().length() == 1)
                    ed2.requestFocus();

                break;
            case R.id.ed2:
                if (ed2.getText().toString().length() == 1)
                    ed3.requestFocus();
                break;
            case R.id.ed3:
                if (ed3.getText().toString().length() == 1)
                    ed4.requestFocus();
                break;
            case R.id.ed4:
                break;
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

        if (sb.length() == 0) {

            ed1.requestFocus();
        }


    }
}
