package com.gslab.gsc_30184.hero.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;

import com.gslab.gsc_30184.hero.R;


public class DashboardActivity extends AppCompatActivity {
    private static final String TAG = "DashboardActivity";
    boolean IsGrid = false;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;
    private Button btnSubmit;
    private String EditFlag, position, user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        recyclerView = findViewById(R.id.recyclerView);
        btnSubmit = findViewById(R.id.btn_submit);
        SharedPreferences prefs = getSharedPreferences("User", MODE_PRIVATE);
        user = prefs.getString("user", null);//"No name defined" is the default value.
        Log.d(TAG, "onCreate: NAME: " + user);


        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        EditFlag = getIntent().getStringExtra("EditFlag");
        position = getIntent().getStringExtra("position");

        //bindAdapter();


    }


}
